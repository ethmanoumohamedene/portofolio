// Exemple : Changer la couleur du texte au survol de la souris
const elements = document.querySelectorAll('.project'); // Sélectionne tous les éléments de classe "project"

elements.forEach((element) => {
  element.addEventListener('mouseover', () => {
    element.style.color = 'red'; // Change la couleur du texte au survol
  });

  element.addEventListener('mouseout', () => {
    element.style.color = ''; // Rétablit la couleur par défaut à la fin du survol
  });
});

// script.js

// script.js

document.addEventListener("DOMContentLoaded", function() {
  var accordions = document.getElementsByClassName("accordion");

  for (var i = 0; i < accordions.length; i++) {
      accordions[i].addEventListener("click", function() {
          this.classList.toggle("active");
          var panel = this.nextElementSibling;
          if (panel.style.maxHeight) {
              panel.style.maxHeight = null;
          } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
          }
      });
  }
});




